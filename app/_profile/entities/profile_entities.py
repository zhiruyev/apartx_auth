from dataclasses import dataclass


@dataclass
class RateInputEntity:
    username: str
    rate: int
