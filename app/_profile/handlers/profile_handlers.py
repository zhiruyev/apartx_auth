import operator

from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group

from app._auth.constants import EMPLOYEE_ROLE
from app._auth.handlers.auth_handlers import AuthHandler
from app._profile.entities.profile_entities import RateInputEntity
from app._profile.models import OwnerRate, EmployeeRate

User = get_user_model()


class ProfileHandler:

    def rate_owner(self, input_entity: RateInputEntity) -> None:
        user = User.objects.get(username=input_entity.username)
        OwnerRate.objects.create(
            user=user,
            rate=input_entity.rate
        )

    def rate_employee(self, input_entity: RateInputEntity) -> None:
        user = User.objects.get(username=input_entity.username)
        EmployeeRate.objects.create(
            user=user,
            rate=input_entity.rate
        )

    def employees_rating(self) -> dict:
        emp_group = Group.objects.get(name=EMPLOYEE_ROLE)
        users = User.objects.filter(groups=emp_group)
        handler = AuthHandler()

        user_rating = {}

        for user in users:
            user_rating[user.username] = handler.get_rate(user)

        sorted_users = dict(sorted(user_rating.items(), key=operator.itemgetter(1), reverse=True))
        my_list = []
        for i in sorted_users.keys():
            my_list.append({'user': i, 'rating': sorted_users[i]})

        return my_list
