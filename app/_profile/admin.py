from django.contrib import admin

from app._profile.models import (
    OwnerRate,
    EmployeeRate,
)


@admin.register(OwnerRate)
class OwnerRateAdmin(admin.ModelAdmin):
    list_display = ["user"]


@admin.register(EmployeeRate)
class EmployeeRateAdmin(admin.ModelAdmin):
    list_display = ["user"]
