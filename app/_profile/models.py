from django.db import models
from django.contrib.auth import get_user_model
from django.utils.translation import gettext as _


User = get_user_model()


class OwnerRate(models.Model):
    user = models.ForeignKey(
        User,
        on_delete=models.SET_NULL,
        null=True
    )
    rate = models.IntegerField()

    class Meta:
        verbose_name = _("Оценка заказчика")
        verbose_name_plural = _("Оценки заказчиков")


class EmployeeRate(models.Model):
    user = models.ForeignKey(
        User,
        on_delete=models.SET_NULL,
        null=True
    )
    rate = models.IntegerField()

    class Meta:
        verbose_name = _("Оценка исполнителя")
        verbose_name_plural = _("Оценки исполнителей")
