from rest_framework import serializers

from app._profile.entities.profile_entities import RateInputEntity
from app._profile.handlers.profile_handlers import ProfileHandler
from app.libs.serialziers import BaseSerializer


class RateInputSerializer(BaseSerializer):
    username = serializers.CharField(max_length=100)
    rate = serializers.IntegerField(min_value=1, max_value=5)

    def create(self, validated_data):
        return RateInputEntity(**validated_data)
