from rest_framework.routers import DefaultRouter

from app._profile.views.profile_views import (
    ProfileViewSet
)

app_name = "_profile"

router = DefaultRouter()

router.register(
    "profile",
    ProfileViewSet,
    basename="profile"
)

urlpatterns = router.get_urls()
