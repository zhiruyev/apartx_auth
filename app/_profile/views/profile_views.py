from rest_framework.decorators import action
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet

from django.contrib.auth import get_user_model

from app._profile.handlers.profile_handlers import ProfileHandler
from app._profile.serializers.profile_serializers import (
    RateInputSerializer,
)

User = get_user_model()


class ProfileViewSet(
    GenericViewSet,
):
    def get_serializer_class(self):
        return {
            "rate_owner": RateInputSerializer,
            "rate_employee": RateInputSerializer,
        }[self.action]

    permission_classes = [IsAuthenticated]

    @action(methods=["post"], detail=False)
    def rate_owner(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        input_entity = serializer.save()

        ProfileHandler().rate_owner(
            input_entity=input_entity
        )

        return Response()

    @action(methods=["post"], detail=False)
    def rate_employee(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        input_entity = serializer.save()

        ProfileHandler().rate_employee(
            input_entity=input_entity
        )

        return Response()

    @action(methods=["get"], detail=False)
    def employees_rating(self, request, *args, **kwargs):
        response = ProfileHandler().employees_rating()

        return Response(response)
