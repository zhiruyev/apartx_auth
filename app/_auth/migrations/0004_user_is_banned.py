# Generated by Django 4.0.6 on 2023-06-25 05:44

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('_auth', '0003_remove_user_avatar_avatarfile'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='is_banned',
            field=models.BooleanField(default=False),
        ),
    ]
