from django.urls import path
from rest_framework.routers import DefaultRouter

from .views.auth_views import (
    register,
    UsersViewSet,
    AvatarViewSet,
)

app_name = "_auth"

urlpatterns = [
    path("register/", register)
]

router = DefaultRouter()

router.register(
    "users",
    UsersViewSet,
    basename="users"
)
router.register(
    "avatar_files",
    AvatarViewSet,
    basename="avatar_files"
)

urlpatterns += router.get_urls()
