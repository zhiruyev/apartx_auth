import uuid

from django.contrib.auth.models import AbstractUser
from django.db import models
from django.utils.translation import gettext as _

from app._auth.constants import FileType
from app._auth.utils.file_upload import file_avatars_upload


class User(AbstractUser):
    rating = models.IntegerField(null=True, blank=True)
    description = models.TextField(null=True)
    is_banned = models.BooleanField(default=False)


class AvatarFile(models.Model):
    uuid = models.UUIDField(default=uuid.uuid4, primary_key=True, editable=False)
    user = models.OneToOneField(
        User,
        on_delete=models.SET_NULL,
        null=True,
        related_name='avatar'
    )
    type = models.PositiveSmallIntegerField(choices=FileType.choices(), default=FileType.TYPE_IMAGE)
    file = models.FileField(upload_to=file_avatars_upload)

    class Meta:
        verbose_name = _("Аватар пользователя")
        verbose_name_plural = _("Аватары пользователей")
