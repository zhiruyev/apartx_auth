from app.libs.base_enum import BaseEnum

OWNER_ROLE = "OWN01"
EMPLOYEE_ROLE = "EMP01"

AVAILABLE_ROLES = [OWNER_ROLE, EMPLOYEE_ROLE]


class FileType(BaseEnum):
    TYPE_IMAGE = 1
