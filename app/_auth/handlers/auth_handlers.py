from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group
from django.db.models import Avg

from rest_framework.exceptions import PermissionDenied

from app._auth.constants import OWNER_ROLE
from app._auth.entities.auth_entities import (
    RegisterInputEntity,
    ChooseRoleInputEntity,
    SetDescriptionInputEntity,
    UploadAvatarInputEntity,
    InfoByNameInputEntity,
)
from app._profile.models import OwnerRate, EmployeeRate

User = get_user_model()


class AuthHandler:

    def register(self, input_entity: RegisterInputEntity) -> User:
        if User.objects.filter(username=input_entity.username).exists():
            raise PermissionDenied("User with this username exists")

        user = User(username=input_entity.username)
        user.set_password(input_entity.password)
        user.save()
        return user

    def choose_role(self, user: User, input_entity: ChooseRoleInputEntity):
        user.groups.clear()

        role = Group.objects.filter(name=input_entity.role).first()
        if role:
            role.user_set.add(user)
        else:
            raise PermissionDenied("No such role")

    def get_rate(self, user: User) -> float:
        user_role_names = [str(i) for i in user.groups.all()]
        if OWNER_ROLE in user_role_names:
            result = OwnerRate.objects.filter(user=user).aggregate(Avg('rate'))
            return result['rate__avg']

        result = EmployeeRate.objects.filter(user=user).aggregate(Avg('rate'))
        return result['rate__avg']

    def set_description(
        self,
        user: User,
        input_entity: SetDescriptionInputEntity
    ) -> None:
        user.description = input_entity.description
        user.save()
