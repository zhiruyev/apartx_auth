from dataclasses import dataclass


@dataclass
class RegisterInputEntity:
    username: str
    password: str


@dataclass
class RegisterResponseEntity:
    user_id: int


@dataclass
class ChooseRoleInputEntity:
    role: str


@dataclass
class SetDescriptionInputEntity:
    description: str


@dataclass
class UploadAvatarInputEntity:
    avatar: bytes


@dataclass
class InfoByNameInputEntity:
    username: str
