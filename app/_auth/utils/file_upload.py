from app.libs.randomize import get_random_string


def file_avatars_upload(instance, filename):
    y = get_random_string(25)
    extension = filename.split(".")[-1]
    return f"users/avatars/{y}.{extension}"
