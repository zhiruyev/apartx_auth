from rest_framework_simplejwt.serializers import TokenObtainPairSerializer
from rest_framework_simplejwt.views import TokenObtainPairView


class MyTokenObtainPairSerializer(TokenObtainPairSerializer):
    @classmethod
    def get_token(cls, user):
        token = super().get_token(user)

        token['groups'] = [str(i) for i in user.groups.all()]
        token['username'] = user.username
        token['is_banned'] = user.is_banned

        return token
