from rest_framework import serializers

from app._auth.handlers.auth_handlers import AuthHandler
from app._auth.models import AvatarFile
from app.libs.serialziers import BaseSerializer
from app._auth.entities.auth_entities import (
    RegisterInputEntity,
    ChooseRoleInputEntity,
    SetDescriptionInputEntity,
    UploadAvatarInputEntity,
    InfoByNameInputEntity,
)

from django.contrib.auth import get_user_model


User = get_user_model()


class RegisterInputSerializer(BaseSerializer):
    username = serializers.CharField()
    password = serializers.CharField(min_length=8, max_length=30, required=True)

    def create(self, validated_data) -> RegisterInputEntity:
        return RegisterInputEntity(**validated_data)


class RegisterResponseSerializer(BaseSerializer):
    user_id = serializers.IntegerField(source="id")


class ChooseRoleInputSerializer(BaseSerializer):
    role = serializers.CharField(max_length=100)

    def create(self, validated_data):
        return ChooseRoleInputEntity(**validated_data)


class SetDescriptionInputSerializer(BaseSerializer):
    description = serializers.CharField(max_length=200)

    def create(self, validated_data):
        return SetDescriptionInputEntity(**validated_data)


class UploadAvatarInputSerializer(BaseSerializer):
    avatar = serializers.FileField()

    def create(self, validated_data):
        return UploadAvatarInputEntity(**validated_data)


class InfoOutputSerializer(serializers.Serializer):
    rate = serializers.SerializerMethodField()
    username = serializers.SerializerMethodField()
    description = serializers.SerializerMethodField()
    avatar = serializers.SerializerMethodField()

    class Meta:
        fields = [
            'username',
            'description',
            'avatar',
            'rate',
        ]

    def get_username(self, obj):
        return self.context['request'].user.username

    def get_description(self, obj):
        return self.context['request'].user.description

    def get_avatar(self, obj):

        if AvatarFile.objects.filter(user=self.context['request'].user).exists():
            request = self.context['request']
            ava = self.context['request'].user.avatar
            return request.build_absolute_uri(ava.file.url)

        return ""

    def get_rate(self, obj):
        user = self.context['request'].user
        return AuthHandler().get_rate(user=user)


class InfoByNameOutputSerializer(serializers.SerializerMethodField):
    rate = serializers.SerializerMethodField()
    username = serializers.SerializerMethodField()
    description = serializers.SerializerMethodField()
    avatar = serializers.SerializerMethodField()

    class Meta:
        fields = [
            'username',
            'description',
            'avatar',
            'rate',
        ]

    def get_username(self, obj):
        return self.initial_data['user'].username

    def get_description(self, obj):
        return self.initial_data['user'].description

    def get_avatar(self, obj):

        if AvatarFile.objects.filter(user=self.initial_data['user']).exists():
            ava = self.initial_data['user'].avatar
            return "http://206.189.61.25:8002/" + ava.file.url

        return ""

    def get_rate(self, obj):
        return AuthHandler().get_rate(user=self.initial_data['user'])


class InfoByNameInputSerializer(BaseSerializer):
    username = serializers.CharField(max_length=200)

    def create(self, validated_data):
        return InfoByNameInputEntity(**validated_data)


class AvatarFileCreateSerializer(serializers.ModelSerializer):
    user = serializers.HiddenField(default=serializers.CurrentUserDefault())

    class Meta:
        model = AvatarFile
        fields = [
            'uuid',
            'type',
            'file',
            'user'
        ]

        read_only_fields = ['uuid']
