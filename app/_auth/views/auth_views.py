from rest_framework.decorators import api_view, action, authentication_classes
from rest_framework.response import Response
from rest_framework import status, generics
from rest_framework.viewsets import GenericViewSet

from app._auth.models import AvatarFile
from app._auth.serializers.auth_serializers import (
    RegisterInputSerializer,
    RegisterResponseSerializer,
    ChooseRoleInputSerializer,
    SetDescriptionInputSerializer,
    UploadAvatarInputSerializer,
    InfoOutputSerializer, AvatarFileCreateSerializer, InfoByNameInputSerializer, InfoByNameOutputSerializer,
)
from app._auth.handlers.auth_handlers import AuthHandler
from django.contrib.auth import get_user_model

from app._auth.utils.authorizations import ApartXBasicAuthorization

User = get_user_model()


@api_view(["POST"])
@authentication_classes([ApartXBasicAuthorization])
def register(request):
    serializer = RegisterInputSerializer(data=request.data)
    serializer.is_valid(raise_exception=True)

    input_entity = serializer.save()
    response = AuthHandler().register(input_entity)

    return Response(RegisterResponseSerializer(response).data, status=status.HTTP_201_CREATED)


class UsersViewSet(
    GenericViewSet,
):
    queryset = User.objects.all()

    def get_serializer_class(self):
        return {
            "choose_role": ChooseRoleInputSerializer,
            "set_description": SetDescriptionInputSerializer,
            "upload_avatar": UploadAvatarInputSerializer,
            "info": InfoOutputSerializer,
            "info_by_name": InfoByNameInputSerializer,
        }[self.action]

    @action(methods=["post"], detail=False, url_path="role")
    def choose_role(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        input_entity = serializer.save()
        user = self.request.user

        AuthHandler().choose_role(user=user, input_entity=input_entity)

        return Response()

    @action(methods=["post"], detail=False)
    def set_description(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        input_entity = serializer.save()
        user = self.request.user

        AuthHandler().set_description(user=user, input_entity=input_entity)

        return Response()

    @action(methods=["get"], detail=False)
    def info(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        return Response(serializer.data)

    @action(methods=["get"], detail=False)
    def info_by_name(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.query_params)
        serializer.is_valid(raise_exception=True)
        input_entity = serializer.save()

        user = User.objects.get(username=input_entity.username)

        serializers = InfoByNameOutputSerializer(data={'user': user})
        serializers.is_valid(raise_exception=True)

        return Response(serializers.data)


class AvatarViewSet(
    generics.CreateAPIView,
    GenericViewSet,
):
    queryset = AvatarFile.objects.all()

    def get_serializer_class(self):
        return {
            "create": AvatarFileCreateSerializer,
        }[self.action]

    def perform_create(self, serializer):
        avatar = AvatarFile.objects.filter(user=self.request.user)
        if avatar.exists():
            avatar.delete()

        serializer.save()
